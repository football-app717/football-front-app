import React from 'react';
import {View} from "react-native";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from "../screens/HomeScreen";
import ProfileScreen from "../screens/ProfileScreen";
import SearchScreen from "../screens/SearchScreen";
import SavedScreen from "../screens/SavedScreen";
import {Foundation, Feather, FontAwesome, Ionicons,MaterialIcons} from '@expo/vector-icons';
import TabHeaderRow from "../components/TabHeaderRow";


const Tab = createBottomTabNavigator();

const TabNavigator = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarActiveTintColor: '#46DE99',
                tabBarShowLabel: false,
                tabBarStyle: { backgroundColor: 'white', height: 50 },
                header: () => (
                    <TabHeaderRow />
                )
            }}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Foundation name="home" size={size} color={color} />
                    )
                }}
            />
            <Tab.Screen
                name="Search"
                component={SearchScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <MaterialIcons name="calendar-today" size={size} color={color} />
                    )
                }}
            />
            <Tab.Screen
                name="Saved"
                component={SavedScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <Feather name="bookmark" size={size} color={color} />
                    )
                }}
            />
            <Tab.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    tabBarIcon: ({color, size}) => (
                        <FontAwesome name="user-o" size={size} color={color} />
                    )
                }}
            />
        </Tab.Navigator>
    );
};

export default TabNavigator;