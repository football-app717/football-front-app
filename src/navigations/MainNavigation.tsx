import React, {useEffect, useState} from 'react';
import {View} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import TabNavigator from "./TabNavigator";
import StackNavigator from "./StackNavigator";
import {useDispatch, useSelector} from "react-redux";
import SplashLoadingScreen from "../screens/SplashLoadingScreen";
import {fetchAboutMe, setLoading, setToken} from "../plugins/redux/slices/auth";
import AsyncStorage from "@react-native-async-storage/async-storage";


const Stack = createStackNavigator();
const MainNavigation = () => {
    const dispatch:any = useDispatch();

    const {token, loading} = useSelector((state: any) => state.auth)

    useEffect(() => {
        console.log("Check auth in main navigation started");
        const checkAuth = async () => {
            dispatch(setLoading(true));
            const token = await AsyncStorage.getItem('token');
            if(token) {
                dispatch(setToken(JSON.parse(token)));
                dispatch(setLoading(false));
            } else {
                dispatch(setToken(''));
                dispatch(setLoading(false));
            }
            // fetchAbout me berasan
            dispatch(fetchAboutMe())
        }


        checkAuth();
    }, []);


    return (
        <NavigationContainer>
            {loading ? (
                <SplashLoadingScreen />
            ) : (
                <Stack.Navigator>
                    {token ? (
                        <>
                            <Stack.Screen
                                name="MainNavigation"
                                component={TabNavigator}
                                options={{ headerShown: false }}
                            />
                        </>
                    ) : (
                        <Stack.Screen
                            name="AuthNavigator"
                            component={StackNavigator}
                            options={{ headerShown: false }}
                        />
                    )}
                </Stack.Navigator>
            )}
        </NavigationContainer>
    );
};

export default MainNavigation;