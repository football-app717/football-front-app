import React from 'react';
import {View} from "react-native";
import { createStackNavigator } from '@react-navigation/stack';
import IntroScreen from "../screens/IntroScreen";
import SignUpScreen from "../screens/SignUpScreen";
import StackHeaderBack from "../components/StackHeaderBack";
import SignInScreen from "../screens/SignInScreen";

const Stack = createStackNavigator();

const StackNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={(props) => ({
                header: () => <StackHeaderBack {...props} />
            })}
        >
            <Stack.Screen
                name="Intro"
                options={{
                    headerShown: false,
                }}

            >
                {(props) => <IntroScreen {...props} />}
            </Stack.Screen>
            <Stack.Screen
                name="SignUp"
            >
                {(props) => <SignUpScreen {...props} />}
            </Stack.Screen>
            <Stack.Screen
                name="SignIn"
            >
                {(props) => <SignInScreen {...props} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default StackNavigator;