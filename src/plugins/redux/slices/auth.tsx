import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import axios from "../axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {date} from "yup";


type fetchTokenGettingData = {
    email: string,
    password: string
};

type pushTokenGettingData = {
    username: string,
    email: string,
    password: string
};


const initialState = {
    user: null,
    token: '',
    loading: false
};

export const fetchToken = createAsyncThunk(
    "auth/fetchToken",
    async ({email, password}:fetchTokenGettingData , {rejectWithValue}) => {
        try {
            const response = await axios.post('/users/auth', {email, password});
            return response.data;

        } catch (error: any) {
            console.log("Error", error.response.data)
            return rejectWithValue(error.response.data);
        }
    }
);
export const pushToken = createAsyncThunk(
    'auth/pushToken',
    async ({username, email, password}: pushTokenGettingData, {rejectWithValue}) => {
        try {
            const response = await axios.post('/users', {username, email, password});
            return response.data;
        } catch (err: any) {
            console.log("Error: ", err.response.data);
            return rejectWithValue(err.response.data);
        }
    }
);



export const fetchAboutMe  = createAsyncThunk(
    'auth/fetchAboutMe',
    async (_, {rejectWithValue}) => {
        try {
            const response = await axios.get('/users/about_me');

            return response.data;
        } catch (err: any) {
            console.log("Erorr: ", err.response.data);
            return rejectWithValue(err.response.data);
        }
    }
)






const authSlices = createSlice({
    name: "auth",
    initialState,
    reducers: {
        setToken: (state, action) => {
            state.token = action.payload;
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder
            //fetchToken start
            .addCase(fetchToken.pending, (state) => {
                state.user = null;
            })
            .addCase(fetchToken.fulfilled, (state, action) => {
                // Store the token in AsyncStorage
                AsyncStorage.setItem("token", JSON.stringify(action.payload.token)).then(
                    () => {
                        console.log("Added to AsyncStorage");
                    }
                );
                console.log(action.payload);
                state.user = action.payload;
                state.token = action.payload.token;
            })
            .addCase(fetchToken.rejected, (state) => {
                state.user = null;
            })
            //fetchToken end


            //pushToken start
            .addCase(pushToken.pending, (state) => {
                state.user = null;
            })
            .addCase(pushToken.fulfilled, (state, action) => {
                // Store the token in AsyncStorage
                AsyncStorage.setItem("token", JSON.stringify(action.payload.token)).then(
                    () => {
                        console.log("Added to AsyncStorage");
                    }
                );
                console.log(action.payload);
                state.user = action.payload;
                state.token = action.payload.token;
            })
            .addCase(pushToken.rejected, (state) => {
                state.user = null;
            })
            //pushToken end


            //fetchAboutMe start
            .addCase(fetchAboutMe.pending, (state) => {
                state.user = null;
            })
            .addCase(fetchAboutMe.fulfilled, (state, action) => {
                console.log(action.payload)
                state.user = action.payload;
            })
            .addCase(fetchAboutMe.rejected, (state, action) => {
                if(action.payload == 'JWT token is not authorized') {
                    AsyncStorage.removeItem('token');
                    state.token = '';
                    state.user = null;
                } else if(action.payload == 'JWT token has expired') {
                    AsyncStorage.removeItem('token');
                    state.token = '';
                    state.user = null;
                }
                state.user = null;
            })
        //fetchAboutMe end
    }
});




export const {setToken, setLoading} = authSlices.actions;

export const authReducer = authSlices.reducer;