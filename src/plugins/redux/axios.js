import axios from 'axios';
import AsyncStorage from "@react-native-async-storage/async-storage";

const instance = axios.create({
    baseURL: 'http://192.168.0.103:3000',
});

instance.interceptors.request.use(async (config) => {
    const token = await AsyncStorage.getItem('token');
    const parsedToken = JSON.parse(token)
    if (token) {
        config.headers['Authorization'] = `Bearer `+ parsedToken;
    }
    return config;
});

export default instance;