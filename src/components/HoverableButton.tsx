import React, { useState } from 'react';
import { TouchableHighlight, Text, StyleSheet } from 'react-native';

const HoverableButton = () => {
    const [isPressed, setIsPressed] = useState(false);

    const handlePressIn = () => {
        setIsPressed(true);
    };

    const handlePressOut = () => {
        setIsPressed(false);
    };

    const buttonStyles = [
        styles.button,
        isPressed && styles.buttonPressed,
    ];

    const textStyles = [
        styles.text,
        isPressed && styles.textPressed,
    ];

    return (
        <TouchableHighlight
            style={buttonStyles}
            onPress={() => console.log('Login button pressed')}
            onPressIn={handlePressIn}
            onPressOut={handlePressOut}
            underlayColor="red"
        >
            <Text style={textStyles}>Login</Text>
        </TouchableHighlight>
    );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#f2f2f2',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
    },
    buttonPressed: {
        backgroundColor: 'red',
    },
    text: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 16,
    },
    textPressed: {
        color: '#fff',
    },
});

export default HoverableButton;