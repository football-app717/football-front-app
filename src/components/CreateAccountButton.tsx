import React, {useState} from 'react';
import {StyleSheet, Text, TouchableHighlight, TouchableOpacity, View} from "react-native";

const CreateAccountButton = ({navigation}: {navigation: any}) => {
    const [isPressed, setIsPressed] = useState(false);


    const handlePressIn = () => {
        setIsPressed(true);
    };

    const handlePressOut = () => {
        setIsPressed(false);
    };


    const buttonStyles = [
        styles.createAccountButton,
        isPressed && styles.buttonPressed,
    ];

    const textStyles = [
        styles.createAccountButtonText,
        isPressed && styles.textPressed,
    ];

    return (
        <>
            <TouchableHighlight
                style={buttonStyles}
                onPress={() => navigation.replace("SignUp")}
                onPressIn={handlePressIn}
                onPressOut={handlePressOut}
                underlayColor="#e7e7e7"
            >
                <Text style={textStyles}>Create an account</Text>
            </TouchableHighlight>
        </>
    );
};

const styles = StyleSheet.create({
    createAccountButton: {
        backgroundColor: 'white',
        width: 318,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 20
    },
    createAccountButtonText: {
        fontSize: 20,
        alignItems: 'center',
        fontWeight: '600'

    },
    buttonPressed: {
        backgroundColor: 'red',
    },
    textPressed: {
        color: 'black',
    },
})

export default CreateAccountButton;