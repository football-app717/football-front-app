import React from 'react';
import {StatusBar, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import { Ionicons } from '@expo/vector-icons';

const StackHeaderBack = ({navigation}: {navigation: any}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.replace('Intro')}>
                <Ionicons name="arrow-back" size={40} color="#46DE99" style={styles.icon} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 95,
        backgroundColor: '#fff',
        paddingTop: 25,
    },
    icon: {
        paddingLeft: 15,
        paddingTop: 15,
    }
})

export default StackHeaderBack;