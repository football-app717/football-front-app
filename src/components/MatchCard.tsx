import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

const MatchCard = () => {
    return (
        <View style={styles.card}>
            <View style={styles.imageCon}>
                <Image source={require('../../assets/Giannis-Zographos-English-Football-Club-Aston-Villa.256.png')} style={styles.imageClub} />
                <Image source={require('../../assets/Giannis-Zographos-English-Football-Club-Arsenal-FC.256.png')}  style={styles.imageClub}/>
            </View>
            <View style={styles.dataCon}>
                <Text style={styles.textDate}>Today</Text>
                <Text style={styles.timeDate}>20:30</Text>
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    card: {
        width: 104,
        height: 100,
        backgroundColor: '#fff',
        marginRight: 10,
        padding: 10,
        borderRadius: 8,
        marginLeft: 5,
        borderWidth: 1,
        borderColor: '#ccc',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    imageCon: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageClub: {
        width: 35,
        height: 35
    },
    dataCon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5
    },
    textDate: {
        fontWeight: '500'
    },
    timeDate: {
        fontWeight: '400',
        fontSize: 13,
        paddingTop: 3
    }
})

export default MatchCard;