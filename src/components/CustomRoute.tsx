import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

const CustomRoute = ({title}: { title: string }) => {
    return (
        <View style={styles.sceneContent}>
            <View style={styles.sceneMainCon}>
                <View style={styles.header}>
                    <Image source={require('../../assets/laliga_109475.png')} style={styles.leagueLog}/>
                    <View style={styles.leagueParahraph}>
                        <Text style={styles.leagueText}>Spain: La Liga</Text>
                    </View>
                </View>

                <View style={styles.singleMatch}>
                    <View style={styles.gameClubImgCon}>
                        <View style={styles.leftCol}>
                            <View style={styles.singleGameClubCon}>
                                <Image
                                    source={require('../../assets/Giannis-Zographos-English-Football-Club-Arsenal-FC.256.png')}
                                    style={[styles.gameClubImg, {marginBottom: 5}]}/>
                                <Text style={styles.gameClubName}>Arsenal</Text>
                            </View>
                            <View style={styles.singleGameClubCon}>
                                <Image
                                    source={require('../../assets/Giannis-Zographos-English-Football-Club-Aston-Villa.256.png')}
                                    style={[styles.gameClubImg, {marginTop: 5}]}/>
                                <Text style={styles.gameClubName}>Aston Villa</Text>
                            </View>
                        </View>

                        <View style={styles.middleCol}>
                            <View style={styles.singleGameScore}>
                                <Text style={[styles.singleGameScoreText, {marginBottom: 5}]}>1</Text>
                                <Text style={[styles.singleGameScoreText, {marginTop: 5}]}>3</Text>
                            </View>
                        </View>

                        <View style={styles.rightCol}>
                            <View style={styles.singleTimeAndDesc}>
                                <Text style={styles.singleTimeAndDescText}>Game is over</Text>
                            </View>
                        </View>
                    </View>
                </View>


            </View>

        </View>


    );
};


const styles = StyleSheet.create({
    sceneContent: {
        padding: 10,
        backgroundColor: '#fff',
        flex: 1,
    },
    sceneMainCon: {
        backgroundColor: '#fff',
        borderRadius: 10,
        paddingHorizontal: 5,
        paddingTop: 5,
        paddingBottom: 15,
        elevation: 20,
        shadowColor: '#979797',
        borderWidth: 1,
        borderColor: '#979797',
    },
    leagueLog: {
        width: 40,
        height: 40,
    },
    leagueParahraph: {
        paddingLeft: 8
    },
    leagueText: {
        fontWeight: '700',
        fontFamily: 'sans-serif-condensed',

    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        paddingVertical: 10,
    },
    leftCol: {
        width: '33.33%',
    },
    middleCol: {
        width: '33.33%',
    },
    rightCol: {
        width: '33.33%'
    },
    singleMatch: {
        borderBottomWidth: 1,
        borderColor: '#ccc'
    },


    singleGameClubCon: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    gameClubImg: {
        width: 25,
        height: 25,
    },
    gameClubImgCon: {
        paddingVertical: 13,
        flexDirection: 'row'
    },
    gameClubName: {
        paddingLeft: 13,
        fontFamily: 'sans-serif-condensed',
    },

    singleGameScore: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    singleGameScoreText: {
        fontWeight: '700',
        fontFamily: 'sans-serif-condensed',
    },

    singleTimeAndDesc: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    singleTimeAndDescText: {
        fontWeight: '700',
        fontFamily: 'sans-serif-condensed',
        color: '#ccc'
    }
});

export default CustomRoute;