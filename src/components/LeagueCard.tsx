import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";

const LeagueCard = () => {
    return (
        <View style={styles.card}>
            <TouchableOpacity style={styles.button}>
                    <View style={styles.imageCon}>
                        <Image source={require('../../assets/laliga_109475.png')} style={styles.imageClub} />
                    </View>
            </TouchableOpacity>
        </View>
    );
};


const styles = StyleSheet.create({
    card: {
        width: 104,
        height: 100,
        backgroundColor: '#fff',
        marginRight: 10,
        padding: 10,
        borderRadius: 8,
        marginLeft: 5,
        borderWidth: 1,
        borderColor: '#ccc', //#46DE99 when it is active
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    imageCon: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    imageClub: {
        width: 55,
        height: 55
    },
    dataCon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5
    },
    textDate: {
        fontWeight: '500'
    },
    timeDate: {
        fontWeight: '400',
        fontSize: 13,
        paddingTop: 3
    },
    button: {
        width: '100%',
        height: '100%'
    }
})

export default LeagueCard;