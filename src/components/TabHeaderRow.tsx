import React from 'react';
import {Keyboard, TouchableWithoutFeedback, View, Text, StyleSheet, Image} from "react-native";

const TabHeaderRow = () => {
    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View style={styles.container}>
                <View style={styles.leftView}>
                    <Image source={require('../../assets/Black_White_Minimalist_Leaves_Sketch_Monogram_Logo__2_-removebg-preview.png')} style={{width: 45, height: 45}} />
                    <Text style={styles.title}>ICHDA</Text>
                </View>

                <View style={styles.rightView}>
                    <View style={styles.imageCon}>
                        <Image source={require('../../assets/preview.png')} style={styles.image} />
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};



const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 110,
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        // borderBottomWidth: 1,
        // borderBottomColor: '#ccc',
        paddingTop: 50
    },
    leftView: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'left',
        flexDirection: 'row',
        paddingLeft: 15,
    },
    rightView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    imageCon: {
      paddingRight: 20
    },
    image: {
        width: 30,
        height: 30,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#46DE99'
    },
    title: {
        fontWeight: '900',
        fontSize: 25,
        color: '#46DE99',
        letterSpacing: 1.5,
        textAlign: 'left',
        paddingLeft: 5,

    },
})

export default TabHeaderRow;