import React, {useState} from 'react';
import {StyleSheet, Text, TouchableHighlight} from "react-native";

const LoginAccountButton = ({navigation}: {navigation: any}) => {
    const [isPressed, setIsPressed] = useState(false);


    const handlePressIn = () => {
        setIsPressed(true);
    };

    const handlePressOut = () => {
        setIsPressed(false);
    };


    const buttonStyles = [
        styles.loginButton,
        isPressed && styles.buttonPressed,
    ];

    const textStyles = [
        styles.loginButtonText,
        isPressed && styles.textPressed,
    ];


    return (
        <>
            <TouchableHighlight
                style={buttonStyles}
                onPress={() => navigation.replace('SignIn')}
                onPressIn={handlePressIn}
                onPressOut={handlePressOut}
                underlayColor="#fff"
            >
                <Text style={textStyles}>Login</Text>
            </TouchableHighlight>
        </>
    );
};



const styles = StyleSheet.create({
    loginButton: {
        borderWidth: 3,
        borderColor: 'white',
        width: 318,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 13,
    },
    loginButtonText: {
        fontSize: 20,
        alignItems: 'center',
        fontWeight: '600',
        color: 'white',
    },
    buttonPressed: {
        backgroundColor: 'red',
    },
    textPressed: {
        color: 'black',
    },
})

export default LoginAccountButton;