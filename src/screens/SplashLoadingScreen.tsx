import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

const SplashLoadingScreen = () => {
    return (
        <View style={styles.container}>
            <Image source={require('../../assets/Black_White_Minimalist_Leaves_Sketch_Monogram_Logo-removebg-preview.png')} style={styles.image} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    image: {
        width: 200,
        height: 200
    }
})

export default SplashLoadingScreen;