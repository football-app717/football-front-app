import React, {useState} from 'react';
import {ScrollView, StatusBar, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View} from "react-native";
import {TextInput} from "react-native-paper";
import * as Yup from 'yup';
import {Formik, Form, Field} from 'formik';
import {useDispatch, useSelector} from "react-redux";
import {fetchToken} from "../plugins/redux/slices/auth";


const SignInSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Email is required'),
    password: Yup.string()
        .min(5, 'Password too short!')
        .max(50, 'Password too long!')
        .required('Password is required')
});


const SignInScreen = ({navigation}: { navigation: any }) => {
    const [isPressed, setIsPressed] = useState(false);
    const handlePressIn = () => {
        setIsPressed(true);
    };
    const handlePressOut = () => {
        setIsPressed(false);
    };

    const buttonStyles = [
        styles.createAccountButton,
        isPressed && styles.buttonPressed,
    ];

    const textStyles = [
        styles.createAccountButtonText,
        isPressed && styles.textPressed,
    ];

    const dispatch:any = useDispatch();


    // const {user} = useSelector((state: any) => state.auth);


    return (
        <View style={styles.container}>
            {/*<StatusBar backgroundColor="#fff"/>*/}
            <ScrollView style={{flex: 1}} keyboardShouldPersistTaps="handled">
                <View style={styles.insideCon}>
                    <View style={styles.textCon}>
                        <Text style={styles.title}>Welcome back!</Text>
                        <Text style={styles.description}>Please fill in your email password to login to your
                            account.</Text>
                    </View>


                    <Formik
                        initialValues={{
                            email: '',
                            password: ''
                        }}
                        onSubmit={async values => {
                            console.log(values)
                            await dispatch(fetchToken(values))
                        }}
                        validationSchema={SignInSchema}
                    >
                        {({errors, touched, values, handleChange, handleBlur, handleSubmit}) => (
                            <View style={styles.form}>
                                <TextInput
                                    label="Email"
                                    value={values.email}
                                    onBlur={handleBlur('email')}
                                    mode={'outlined'}
                                    style={styles.input}
                                    onChangeText={handleChange('email')}
                                    theme={{
                                        colors: {
                                            primary: 'black',
                                            text: 'black',
                                            outline: 'black',
                                            placeholder: 'black',
                                            background: '#fff'
                                        }
                                    }}
                                    outlineColor={touched.email && errors.email ? 'red': undefined}
                                    autoCapitalize="none"
                                />
                                {touched.email && errors.email && <Text style={styles.inputError}>{errors.email}</Text>}
                                <TextInput
                                    label="Password"
                                    value={values.password}
                                    onBlur={handleBlur('password')}
                                    mode={'outlined'}
                                    style={styles.input}
                                    onChangeText={handleChange('password')}
                                    theme={{
                                        colors: {
                                            text: 'black',
                                            primary: 'black',
                                            outline: 'black',
                                            placeholder: 'black',
                                            background: '#fff'
                                        }
                                    }}
                                    outlineColor={touched.password && errors.password ? 'red': undefined}
                                    autoCapitalize="none"
                                />
                                {touched.password && errors.password &&
                                    <Text style={styles.inputError}>{errors.password}</Text>}

                                <TouchableHighlight
                                    style={buttonStyles}
                                    onPress={() => handleSubmit()}
                                    onPressIn={handlePressIn}
                                    onPressOut={handlePressOut}
                                    underlayColor="#e7e7e7"
                                >
                                    <Text style={textStyles}>Login</Text>
                                </TouchableHighlight>


                                <View style={styles.help}>
                                    <Text>Don’t have an account?</Text>
                                    <TouchableOpacity onPress={() => navigation.replace('SignUp')}>
                                        <Text style={styles.helpSignUp}>Sign Up</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        )}
                    </Formik>

                </View>
            </ScrollView>
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    insideCon: {
        padding: 15
    },
    textCon: {
        paddingTop: 20
    },
    title: {
        fontSize: 35,
        fontWeight: '800',
        color: '#46DE99'
    },
    description: {
        fontSize: 20,
        paddingTop: 20
    },
    form: {
        flex: 1,
        paddingTop: 70,
    },
    input: {
        paddingVertical: 10,
        justifyContent: "center",
        fontSize: 20,
        marginBottom: 10,
    },
    inputError: {
        marginTop: -8,
        color: 'red'
    },

    createAccountButton: {
        backgroundColor: '#46DE99',
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 20
    },
    createAccountButtonText: {
        fontSize: 20,
        alignItems: 'center',
        fontWeight: '600',
        color: 'white'

    },
    buttonPressed: {
        backgroundColor: 'red',
    },
    textPressed: {
        color: 'black',
    },
    help: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    helpSignUp: {
        color: '#46DE99'
    }
})

export default SignInScreen;