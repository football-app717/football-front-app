import React, {useState} from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import CustomRoute from "../components/CustomRoute";



const initialLayout = {width: Dimensions.get('window').width};

const routes = [
    {key: 'yesterday', title: 'Yesterday'},
    {key: 'today', title: 'Today'},
    {key: 'tomorrow', title: 'Tomorrow'},
    {key: 'afterTomorrow', title: 'May 26, 2022'},
];

const renderScene = ({route}: {route: any}) =>(
    <CustomRoute title={route.title} />
);

const HomeScreen = () => {
    const [index, setIndex] = useState(0);

    const handleIndexChange = (index: any) => {
        setIndex(index);
    };

    const renderTabBar = (props: any) => (
        <TabBar
            {...props}
            indicatorStyle={styles.indicator}
            style={styles.tabBar}
            tabStyle={styles.tab}
            renderLabel={({route, focused, color}) => (
                <Text style={[styles.tabBarLabel, focused && styles.activeTabLabel]}>
                    {route.title}
                </Text>
            )}
            scrollEnabled
            upperCaseLabel={false}
        />
    );

    return (
        <View style={styles.container}>
            <TabView
                navigationState={{index, routes}}
                renderScene={renderScene}
                onIndexChange={handleIndexChange}
                initialLayout={initialLayout}
                renderTabBar={renderTabBar}
                overScrollMode={'auto'}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scene: {
        flex: 1,
    },



    indicator: {
        backgroundColor: '#000', // Change color as needed
        height: 2,
        borderRadius: 20
    },
    tabBar: {
        backgroundColor: '#fff', // Change color as needed
        height: 30,
    },
    tab: {
        width: 120, // Set the width of each tab as needed
    },
    tabBarLabel: {
        color: '#ccc', // Change color as needed
        marginTop: -14,
        fontFamily: 'sans-serif-condensed',
        fontSize: 12,
        textTransform: 'none',
        fontWeight: '700',
    },
    activeTabLabel: {
        color: 'black', // Color for the active label
    },
});

export default HomeScreen;






