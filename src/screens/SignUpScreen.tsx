import React, {useEffect, useState} from 'react';
import {ScrollView, StatusBar, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View} from "react-native";
import {TextInput} from "react-native-paper";
import * as Yup from 'yup';
import {Formik, Form, Field} from 'formik';
import {useDispatch} from "react-redux";
import {pushToken} from "../plugins/redux/slices/auth";


const SignUpSchema = Yup.object().shape({
    username: Yup.string()
        .min(3, 'Username is too short!')
        .max(50, 'Username too long!')
        .required('Username is required'),
    email: Yup.string().email('Invalid email').required('Email is required'),
    password: Yup.string()
        .min(5, 'Password too short!')
        .max(50, 'Password too long!')
        .required('Password is required')
});

const SignUpScreen = ({navigation}: { navigation: any }) => {
    const [isPressed, setIsPressed] = useState(false);
    const handlePressIn = () => {
        setIsPressed(true);
    };

    const handlePressOut = () => {
        setIsPressed(false);
    };


    const buttonStyles = [
        styles.createAccountButton,
        isPressed && styles.buttonPressed,
    ];

    const textStyles = [
        styles.createAccountButtonText,
        isPressed && styles.textPressed,
    ];


    const dispatch:any = useDispatch();

    return (
        <View style={styles.container}>
            {/*<StatusBar backgroundColor="#fff" />*/}
            <ScrollView style={{flex: 1}} keyboardShouldPersistTaps="handled">
                <View style={styles.insideCon}>
                    <View style={styles.textCon}>
                        <Text style={styles.title}>Create an account!</Text>
                        <Text style={styles.description}>Please fill in your details to create your account</Text>
                    </View>


                    <Formik
                        initialValues={{
                            username: '',
                            email: '',
                            password: ''
                        }}
                        onSubmit={values => {
                            console.log(values);
                            dispatch(pushToken(values));
                        }}
                        validationSchema={SignUpSchema}
                    >
                        {({errors, touched, values, handleChange, handleBlur, handleSubmit}) => (
                            <View style={styles.form}>
                                <TextInput
                                    label="Name"
                                    value={values.username}
                                    onBlur={handleBlur('username')}
                                    mode={'outlined'}
                                    style={styles.input}
                                    theme={{
                                        colors: {
                                            primary: 'black',
                                            text: 'black',
                                            outline: 'black',
                                            placeholder: 'black',
                                            background: '#fff'
                                        }
                                    }}
                                    onChangeText={handleChange('username')}
                                    outlineColor={touched.username && errors.username ? 'red': undefined}
                                    autoCapitalize="none"
                                />
                                {touched.username && errors.username && <Text style={styles.inputError}>{errors.username}</Text>}

                                <TextInput
                                    label="Email"
                                    value={values.email}
                                    onBlur={handleBlur('email')}
                                    mode={'outlined'}
                                    style={styles.input}
                                    onChangeText={handleChange('email')}
                                    theme={{
                                        colors: {
                                            primary: 'black',
                                            text: 'black',
                                            outline: 'black',
                                            placeholder: 'black',
                                            background: '#fff'
                                        }
                                    }}
                                    outlineColor={touched.email && errors.email ? 'red': undefined}
                                    autoCapitalize="none"
                                />
                                {touched.email && errors.email && <Text style={styles.inputError}>{errors.email}</Text>}
                                <TextInput
                                    label="Password"
                                    value={values.password}
                                    onBlur={handleBlur('password')}
                                    mode={'outlined'}
                                    style={styles.input}
                                    onChangeText={handleChange('password')}
                                    theme={{
                                        colors: {
                                            text: 'black',
                                            primary: 'black',
                                            outline: 'black',
                                            placeholder: 'black',
                                            background: '#fff'
                                        }
                                    }}
                                    outlineColor={touched.password && errors.password ? 'red': undefined}
                                    autoCapitalize="none"
                                />
                                {touched.password && errors.password &&
                                    <Text style={styles.inputError}>{errors.password}</Text>}

                                <TouchableHighlight
                                    style={buttonStyles}
                                    onPress={() => handleSubmit()}
                                    onPressIn={handlePressIn}
                                    onPressOut={handlePressOut}
                                    underlayColor="#e7e7e7"
                                >
                                    <Text style={textStyles}>Create an account</Text>
                                </TouchableHighlight>


                                <View style={styles.help}>
                                    <Text>You already have an account ! </Text>
                                    <TouchableOpacity onPress={() => navigation.replace('SignIn')}>
                                        <Text style={styles.helpSignIn}>Sign in</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        )}
                    </Formik>
                </View>
            </ScrollView>
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    insideCon: {
      padding: 15
    },
    textCon: {
        paddingTop: 20
    },
    title: {
        fontSize: 35,
        fontWeight: '800',
        color: '#46DE99'
    },
    description: {
        fontSize: 20,
        paddingTop: 20
    },
    form: {
        flex: 1,
        paddingTop: 70,
    },
    input: {
        paddingVertical: 10,
        justifyContent: "center",
        fontSize: 20,
        marginBottom: 10,
    },
    inputError: {
        marginTop: -8,
        color: 'red'
    },

    createAccountButton: {
        backgroundColor: '#46DE99',
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 20
    },
    createAccountButtonText: {
        fontSize: 20,
        alignItems: 'center',
        fontWeight: '600',
        color: 'white'

    },
    buttonPressed: {
        backgroundColor: 'red',
    },
    textPressed: {
        color: 'black',
    },
    help: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    helpSignIn: {
        color: '#46DE99'
    }
})

export default SignUpScreen;