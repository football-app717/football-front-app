import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {Feather, MaterialCommunityIcons, Entypo, MaterialIcons} from '@expo/vector-icons';
import {useSelector} from "react-redux";

const ProfileScreen = () => {
    const {user} = useSelector((state:any) => state.auth)
    return (
        <View style={styles.con}>
            <View style={styles.header}>
                <View style={styles.avatarCon}>
                    <Image source={require('../../assets/preview.png')} style={styles.avatar} />
                    <TouchableOpacity style={styles.edit}>
                        <Feather name="edit-3" size={24} color="white" />
                    </TouchableOpacity>
                </View>
                <View style={styles.fullNameCon}>
                    <Text style={styles.fullNameAdditional}>Hello, </Text>
                    <Text style={styles.fullName}>{user.username}</Text>
                </View>
            </View>

            <View style={styles.main}>
                <View style={styles.card}>
                    <Text style={styles.cardLeftIcon}>
                        <MaterialCommunityIcons name="email-edit-outline" size={24} color="#46DE99" />
                    </Text>
                    <Text style={styles.cardMiddleText}>
                        Contact us
                    </Text>
                    <Text style={styles.cardRightIcon}>
                        <Entypo name="chevron-right" size={24} color="#ccc" />
                    </Text>
                </View>

                <View style={styles.card}>
                    <Text style={styles.cardLeftIcon}>
                        <MaterialIcons name="support-agent" size={24} color="#46DE99" />
                    </Text>
                    <Text style={styles.cardMiddleText}>
                        Support
                    </Text>
                    <Text style={styles.cardRightIcon}>
                        <Entypo name="chevron-right" size={24} color="#ccc" />
                    </Text>
                </View>
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    con: {
        borderTopWidth: 1,
        borderTopColor: '#ccc',
        backgroundColor: '#fff',
        flex: 1,
        padding: 15
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    avatarCon: {
        position: 'relative'
    },
    avatar: {
        width: 135,
        height: 135,
        borderRadius: 70,
    },
    edit: {
        backgroundColor: '#46DE99',
        alignItems: "center",
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 500,
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    fullNameCon: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    fullNameAdditional: {
        fontFamily: 'sans-serif-condensed',
        fontSize: 18
    },
    fullName: {
        fontFamily: 'sans-serif-condensed',
        fontSize: 20,
        fontWeight: '700'
    },
    main: {
        padding: 15
    },
    card: {
        borderWidth: 1,
        borderColor: '#979797',
        borderRadius: 15,
        backgroundColor: '#fff',
        elevation: 20,
        shadowColor: '#979797',
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'center'
        padding: 20,
        marginBottom: 15
    },
    cardLeftIcon: {
        width: '20%',
        textAlign: 'left'
    },
    cardMiddleText: {
        width: '60%',
        textAlign: "left",
        fontFamily: 'sans-serif-condensed',
        fontWeight: "700"
    },
    cardRightIcon: {
        width: '20%',
        textAlign: 'right'
    }
})

export default ProfileScreen;