import React from 'react';
import {Image, StatusBar, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import CreateAccountButton from "../components/CreateAccountButton";
import LoginAccountButton from "../components/LoginAccountButton";
import { useFocusEffect } from '@react-navigation/native';

const IntroScreen = ({navigation}: {navigation: any }) => {
    // StatusBar.setBackgroundColor('#46DE99');
    return (

        <>
            <View style={styles.container}>
                {/*<StatusBar backgroundColor="#46DE99" />*/}
                {/*<StatusBar barStyle="light-content" backgroundColor="#46DE99" />*/}

                <View style={styles.imageCon}>
                    <Image source={require('../../assets/pngegg.png')} style={styles.image} />
                </View>
                <View style={styles.textsCon}>
                    <Text style={styles.title}>We are happy to see you!</Text>
                    <Text style={styles.description}>Welcome to SPOT</Text>
                </View>
                <View>
                    <CreateAccountButton navigation={navigation} />
                    <LoginAccountButton navigation={navigation} />

                </View>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#46DE99',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 80
    },
    textsCon: {
      padding: 10
    },
    title: {
        fontSize: 24,
        fontWeight: '800',
        color: 'white',
        textAlign: 'center',
        paddingBottom: 5
    },
    description: {
        color: 'white',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '600'
    },
    createAccountButton: {
        backgroundColor: 'white',
        width: 318,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 20
    },
    createAccountButtonText: {
        fontSize: 20,
        alignItems: 'center',
        fontWeight: '600'

    },

    loginButton: {
        borderWidth: 3,
        borderColor: 'white',
        width: 318,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 13,
    },
    loginButtonText: {
        fontSize: 20,
        alignItems: 'center',
        fontWeight: '600',
        color: 'white',

    },
    imageCon: {
      marginBottom: 140
    },
    image: {
        width: 120,
        height: 120
    }

})

export default IntroScreen;