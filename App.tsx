import MainNavigation from "./src/navigations/MainNavigation";
import {StatusBar} from "react-native";
import React from "react";
import {Provider} from "react-redux";
import store from "./src/plugins/redux/store";

export default function App() {
  return (
        <Provider store={store}>
            <>
                <MainNavigation />
            </>
        </Provider>
  );
}
